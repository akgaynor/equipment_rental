# -*- coding: utf-8 -*-
# Copyright (c) 2019, Bai Web and Mobile Lab and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Equipment(Document):
	pass


@frappe.whitelist()
def set_equipment_partners(partner, equipment):
	var = frappe.get_doc("Equipment Partners", partner)
	var.equipment = equipment
	var.save()
