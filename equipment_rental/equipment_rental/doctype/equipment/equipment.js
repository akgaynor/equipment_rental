// Copyright (c) 2019, Bai Web and Mobile Lab and contributors
// For license information, please see license.txt

frappe.ui.form.on('Equipment', {
    equipment_item: function(frm) {
        let equipment_item = cur_frm.doc.equipment_item;
        frappe.call({
            method: "frappe.client.get_value",
            args:{
               doctype: "Item Price",
               filters: {
                   item_code: equipment_item
               },
               fieldname: ["price_list_rate"]
            },
            callback: function(r){
               if (r.message){
                   let price_list_rate = r.message.price_list_rate;
                   cur_frm.set_value("equipment_rate", price_list_rate);
               }
            }
        });},
    'refresh': function(frm){
        if (!cur_frm.doc.name.includes("New Equipment")
                && cur_frm.doc.equipment_status == "Available"){
            frm.add_custom_button(__('Book Equipment'), function(){
                let new_booking = frappe.model.get_new_doc(
                    'Equipment Booking');

                let new_roe = frappe.model.get_new_doc(
                    'Rented Out Equipment');

                new_roe.roe_equipment = cur_frm.doc.name;
                new_roe.roe_rate = cur_frm.doc.equipment_rate;

                new_booking.booking_rentals = [new_roe];

                frappe.set_route('Form',
                    'Equipment Booking',
                    new_booking.name);
            });
        }
    },
    'validate': function(frm){
        let equipment_partners = cur_frm.doc.equipment_partners;
        let partners = [];

        if (equipment_partners){
            equipment_partners.forEach(function (equipment_partner){
                if (equipment_partner.employee_id != undefined){
                    partners.push(equipment_partner.employee_id);
                }
            });

            if (partners.length != 0) {
                let unique = new Set(partners);
                if (partners.length !== unique.size)
                    frappe.throw("Duplicate equipment partner!");
            }
        }

        if (cur_frm.doc.name.includes("New Equipment")){
            let promise = frappe.db.exists("Equipment", cur_frm.doc.equipment_item);
            promise.then(exists => {
                if (exists){
                    frappe.validated = false;
                    frappe.throw("Equipment " +cur_frm.doc.equipment_item + " already exists!");
                }
            });
        }
    },
    'after_save': function(frm){
        if (frappe.validated == true){
            let equipment_partners = cur_frm.doc.equipment_partners;
            equipment_partners.forEach(function (equipment_partner){
                if (equipment_partner.employee_id != undefined)
                    frappe.call({
                       method:'equipment_rental.equipment_rental.doctype.equipment.equipment.set_equipment_partners',
                       args:{
                           'partner': equipment_partner.name,
                           'equipment': cur_frm.doc.name
                       },
                        callback: function (r){}
                    });
            });
        }
    }
});

frappe.ui.form.on("Equipment Partners", "employee_id",
    function(frm, cdt, cdn){
        let employee = locals[cdt][cdn];
        let employee_id =  employee.employee_id;
        if (employee_id){
            frappe.call({
               method: "frappe.client.get",
               args:{
                   doctype: "Employee",
                   name: employee_id
               },
               callback(r){
                   if (r.message){
                       let employee_name = r.message.employee_name;
                       frappe.model.set_value(
                           cdt, cdn,
                           "employee_name",
                           employee_name)
                   }
               }
           });
        }
    }
);

