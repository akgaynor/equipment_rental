# -*- coding: utf-8 -*-
# Copyright (c) 2019, Bai Web and Mobile Lab and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from datetime import date
from frappe.utils import date_diff


class EquipmentBooking(Document):
	pass

# GETTER METHODS
@frappe.whitelist()
def get_equipment(equipment):
	return frappe.get_doc("Equipment", equipment)


@frappe.whitelist()
def is_booked(equipment):
	equip = frappe.get_value("Equipment", equipment, "equipment_status")
	if equip == "Booked":
		return True
	else:
		return False


# SETTER METHODS
@frappe.whitelist()
def set_to_booked(equipment):
	to_book = frappe.get_doc("Equipment", equipment)
	to_book.equipment_status = "Booked"
	to_book.save()


def set_to_available(equipment):
	print("inside set_to_available")
	to_free = frappe.get_doc("Equipment", equipment)
	to_free.equipment_status = "Available"
	to_free.save()


@frappe.whitelist()
def set_booking_to_roe(roe, booking):
	to_book = frappe.get_doc("Rented Out Equipment", roe)
	to_book.roe_booking = booking
	to_book.save()


# CRON TASKS
@frappe.whitelist()
def daily_task():
	bookings = frappe.get_all(
		"Equipment Booking",
		fields=[
			'name',
			'booking_customer',
			'booking_invoice_date'
		]
	)

	for booking in bookings:
		roe = frappe.get_all(
			"Rented Out Equipment",
			filters={
				'roe_booking': booking.name
			},
			fields=[
				'roe_equipment',
				'roe_rate',
				'roe_end'
			]
		)

		current_date = date.today()
		if current_date == booking.booking_invoice_date:
			generate_invoice(booking, roe)

			for equipment in roe:
				if current_date == equipment.roe_end:
					set_to_available(equipment.roe_equipment)

# GENERATE TASKS
@frappe.whitelist()
def generate_profit_sharing(booking):
	# Ann fix the design of Equipment Partners and Rented Out Equipment to have
	# equipment and roe_booking respectively
	return get_profits(booking), get_partners(booking)


@frappe.whitelist()
def get_profits(booking):
	return frappe.db.sql("""SELECT roe_equipment, employee_id,
							roe_rate, roe_start, roe_end
							FROM `tabRented Out Equipment`, 
							`tabEquipment Partners`, 
							`tabEquipment`, `tabEquipment Booking` 
							WHERE `tabEquipment Booking`.name = %s AND 
							roe_booking = `tabEquipment Booking`.name AND  
							equipment_item = roe_equipment AND 
							`tabEquipment Partners`.equipment =
							 equipment_item""", booking)


@frappe.whitelist()
def get_partners(booking):
	return frappe.db.sql("""SELECT 
							COUNT(equipment_item), equipment_item,
							DATEDIFF(roe_end, roe_start)+1, 
							roe_rate
							FROM `tabRented Out Equipment`, 
							`tabEquipment Partners`, 
							`tabEquipment`, `tabEquipment Booking` 
							WHERE `tabEquipment Booking`.name = %s AND 
							roe_booking = `tabEquipment Booking`.name AND  
							equipment_item = roe_equipment AND 
							`tabEquipment Partners`.equipment =
							 equipment_item GROUP BY 
							 equipment_item""", booking)


def generate_invoice(booking, roe):
	debit_to = frappe.db.sql("""SELECT name FROM `tabAccount` 
							WHERE name LIKE 'Debtors%'""")
	debit_to = debit_to[0][0]

	income_account = frappe.db.sql("""SELECT name FROM `tabAccount` 
									WHERE name LIKE 'Sales%'""")
	income_account = income_account[0][0]

	items = []
	for equipment in roe:
		item = frappe.get_doc("Item", equipment.roe_equipment)
		equipment = {
			'item_code': item.item_code,
			'description': item.description,
			'qty': date_diff(equipment.roe_end, equipment.roe_start) + 1,
			'uom': 'Nos',
			'rate': equipment.roe_rate,
			'conversion_factor': 1,
			'income_account': income_account
		}
		items.append(equipment)

	new_invoice = frappe.get_doc({
		"doctype": "Sales Invoice",
		"customer": booking.booking_customer,
		"due_date": date.today(),
		"items": items,
		"debit_to": debit_to
	})
	new_invoice.save()

	booking_to_invoice = frappe.get_doc("Equipment Booking", booking)
	booking_to_invoice.invoice = new_invoice
	booking_to_invoice.save()
