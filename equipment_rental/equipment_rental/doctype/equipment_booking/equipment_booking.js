// Copyright (c) 2019, Bai Web and Mobile Lab and contributors
// For license information, please see license.txt

frappe.ui.form.on("Equipment Booking", {
    'setup': function (frm) {
        frm.set_query("roe_equipment", "booking_rentals", function (doc, cdt, cdn) {
            return {
                filters: [
                    ["Equipment", "equipment_status", "=", "Available"]
                ]
            }
        });
    },
    'before_submit': function (frm) {
        // Client-side validation for Rented Out Equipment
        let booking_rentals = cur_frm.doc.booking_rentals;
        let roe = [];

        if (booking_rentals.length > 0){
            booking_rentals.forEach(function catchErrors(booking_rental){
                if (booking_rental.roe_equipment != undefined)
                    roe.push(booking_rental.roe_equipment);
                if (booking_rental.roe_end < booking_rental.roe_start){
                    frappe.throw("Rental Start Date for " +
                        booking_rental.roe_equipment +
                        " cannot be before Rental End Date");
                }
            });

            if (roe.length != 0){
                let unique = new Set(roe);
                if(roe.length !== unique.size){
                    frappe.throw("Duplicate rented out equipment!");
                }
                else{
                    booking_rentals.forEach(function isBooked(booking_rental){
                        frappe.call({
                            method: 'equipment_rental.equipment_rental.doctype.equipment_booking.equipment_booking.is_booked',
                            args: {
                                'equipment': booking_rental.roe_equipment,
                            },
                            callback: function(r){
                                if(r.message == true){
                                    frappe.validated = false;
                                    frappe.throw(booking_rental.roe_equipment + " is already booked!");
                                }
                                else{
                                    frappe.call({
                                        method: 'equipment_rental.equipment_rental.doctype.equipment_booking.equipment_booking.set_to_booked',
                                        args: {
                                            'equipment': booking_rental.roe_equipment
                                        },
                                        callback: function (r){
                                            booking_rental.roe_booking = cur_frm.doc.name;
                                        }
                                    });
                                }
                            }
                        });
                    });
                }
            }
        }
        else
            frappe.throw("At least one rented out equipment is required!");
    },
    'refresh': function (frm){
        if (cur_frm.doc.docstatus == 1){
            frm.add_custom_button(__('Get Profit Report'), function(){
                frappe.route_options = {
                    'booking': cur_frm.doc.name
                };
                frappe.set_route('query-report', 'Equipment Booking',
                    {
                        'booking': cur_frm.doc.name
                    });
            });

            frm.clear_table("booking_profit");
            frappe.call({
                method: 'equipment_rental.equipment_rental.doctype.equipment_booking.equipment_booking.generate_profit_sharing',
                args: {
                    'booking': cur_frm.doc.name
                },
                callback: function(r){
                    let partners = r.message[0];
                    let roe = r.message[1];
                    let profit_percentage = 0;
                    let profit_amount = 0;

                    let no_of_partners = 0;
                    partners.forEach(function (partner){
                        let row = frappe.model.add_child(
                            frm.doc,
                            "Booking Profit Sharing",
                            "booking_profit"
                        );
                        row.profit_equipment = partner[0];
                        row.profit_partner = partner[1];

                        let total_bill = 0;
                        roe.forEach(function (equipment){
                            total_bill += equipment[2] * equipment[3];
                            if(partner[0] == equipment[1])
                                no_of_partners = equipment[0];
                        });

                        let days_rented = frappe.datetime.get_day_diff(
                            partner[4],
                            partner[3]) + 1;

                        profit_percentage = ((
                            (partner[2] * days_rented)
                            /no_of_partners)/total_bill) * 100;
                        profit_amount = (profit_percentage * total_bill) / 100;
                        row.profit_percentage = profit_percentage.toFixed(2);
                        row.profit_amount = profit_amount.toFixed(2);
                        cur_frm.refresh_field("booking_profit");
                        cur_frm.set_value("booking_bill", total_bill);
                    });
                }
            });
        }
    }
});

frappe.ui.form.on("Rented Out Equipment", {
    roe_equipment: function(frm, cdt, cdn){
        let row = frappe.get_doc(cdt, cdn);

        if (row.roe_equipment != undefined){
            frappe.call({
                method: 'equipment_rental.equipment_rental.doctype.equipment_booking.equipment_booking.get_equipment',
                args:{
                    'equipment': row.roe_equipment
                },
                callback: function(r){
                    frappe.model.set_value(cdt, cdn, "roe_rate", r.message.equipment_rate);
                }
            });
        }
    }
});
