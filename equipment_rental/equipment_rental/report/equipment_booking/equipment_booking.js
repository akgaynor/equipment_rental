// Copyright (c) 2016, Bai Web and Mobile Lab and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Equipment Booking"] = {
	"filters": [
		{
			"fieldname": "booking",
			"label":__("Booking"),
			"fieldtype":"Link",
			"width": "80",
			"options": "Equipment Booking"
		},
	]
};
