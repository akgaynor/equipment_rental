# Copyright (c) 2013, Bai Web and Mobile Lab and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
from equipment_rental.equipment_rental.doctype.equipment_booking.equipment_booking import get_profits, get_partners
from frappe.utils import date_diff
import frappe


def execute(filters=None):
    columns = get_columns()
    data = get_data(filters)
    return columns, data


def get_data(filters):
    data = []
    booking = filters.get("booking")
    partners = get_profits(booking)
    roe = get_partners(booking)
    no_of_partners = 0

    for partner in partners:
        total_bill = 0
        for equipment in roe:
            total_bill += equipment[2] * equipment[3]
            if partner[0] == equipment[1]:
                no_of_partners = equipment[0]

        days_rented = date_diff(partner[4], partner[3]) + 1
        print(days_rented)
        print(partner[2])
        print((float((partner[2] * days_rented) / no_of_partners) / total_bill) * 100)
        profit_percentage = (float((partner[2] * days_rented) / no_of_partners) / total_bill) * 100
        profit_amount = (profit_percentage * total_bill) / 100
        print(profit_percentage)
        print(profit_amount)
        row = [partner[1], partner[0], round(profit_percentage, 2), round(profit_amount, 2)]
        print(row)
        data.append(row)

    return data


def get_columns():
    columns = [
        {
            "fieldname": "partner",
            "label": "Partner",
            "fieldtype": "Link",
            "options": "Equipment Partners",
            "width": 300
        },
        {
            "fieldname": "equipment",
            "label": "Equipment",
            "fieldtype": "Link",
            "options": "Equipment",
            "width": 300
        },
        {
            "fieldname": "percentage",
            "label": "Profit %",
            "fieldtype": "Float",
            "width": 300
        },
        {
            "fieldname": "amount",
            "label": "Profit Amount",
            "fieldtype": "Float",
            "width": 300
        },
    ]
    return columns
